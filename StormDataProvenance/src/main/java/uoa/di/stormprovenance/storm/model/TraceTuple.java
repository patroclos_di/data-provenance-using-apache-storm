package uoa.di.stormprovenance.storm.model;

public class TraceTuple {

	private String id;
	private String host;
	private String worker;
	private String threadID;
	private String executor;
	private Object origTuple;

	public TraceTuple(String id, String host, String worker, String threadID, String executor, Object origTuple) {
		super();
		this.id = id;
		this.host = host;
		this.worker = worker;
		this.threadID = threadID;
		this.executor = executor;
		this.origTuple = origTuple;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker = worker;
	}

	public String getThreadID() {
		return threadID;
	}

	public void setThreadID(String threadID) {
		this.threadID = threadID;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	public Object getOrigTuple() {
		return origTuple;
	}

	public void setOrigTuple(Object origTuple) {
		this.origTuple = origTuple;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TraceTuple [");
		if (id != null)
			builder.append("id=").append(id).append(", ");
		if (host != null)
			builder.append("host=").append(host).append(", ");
		if (worker != null)
			builder.append("worker=").append(worker).append(", ");
		if (threadID != null)
			builder.append("threadID=").append(threadID).append(", ");
		if (executor != null)
			builder.append("executor=").append(executor).append(", ");
		if (origTuple != null)
			builder.append("origTuple=").append(origTuple);
		builder.append("]");
		return builder.toString();
	}
}
